# The Git Book Guidelines

1. Fork this repository
2. Clone your fork of this repository
3. Create a feature branch
4. Add a paragraph of text to this document.
5. Commit your change.
6. Push your feature branch to your fork of this repository.
7. Open a merge request from your fork/feature branch to the default branch of this repository.
8. Assign the merge request to a reviewer.
9. Discuss
10. Merge the merge request once it has been approved.
11. Go back to step 1.
