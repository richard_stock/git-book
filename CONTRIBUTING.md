# Contributing

When contributing to this repository, please first discuss the change you wish to make via issue, email, or any other method with the owners of this repository before making a change.

## Initial Setup

To clone the remote repository to your local machine, navigate to the local directory you wish to set up your local Git repository in, and use either of the commands below:

SSH:  

    git clone git@gitlab.com:programming_club/git-book.git

HTTPS:  
    
    git clone https://gitlab.com/programming_club/git-book.git

## Adding the Upstream Repository as a Remote

If you have cloned the remote repository during the initial setup, it will be automatically be added as a remote called `origin`.

To add a new remote Git repository from your **terminal**, run the command:  
`git remote add <shortname> <url>`, where `shortname` is a name you can reference easily, and `url` is the URL of the remote (see URL's in Initial Setup section above).

## Fetching Changes from the Upstream Repository
To get data from your remote, you can run:
`git fetch <remote>`

If you **cloned** the repository, `git fetch origin` fetches any new work that has been pushed to that server since you cloned (or last fetched from) it.   
If you **added** the repository, use the `shortname` you created as `<remote>`.

It’s important to note that the git fetch command only downloads the data to your local repository — it doesn’t automatically merge it with any of your work or modify what you’re currently working on. You have to merge it manually into your work when you’re ready.

## How to Merge Changes into the Upstream Repository
Please refer to: [Merge Requests Documentation | GitLab](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html "How to Create Merge Requests") for a step by step guide on completing a merge request.

To create a merge request from your **terminal**, see:  [Creating Merge Requests from your Terminal](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#new-merge-request-from-your-local-environment)

## Writing in Markdown Syntax

This project uses **Markdown language** for formatting text.  
Please refer to:   [Markdown Syntax Documentation](https://daringfireball.net/projects/markdown/syntax "How to Use Markdown") for guidance on how to use the Markdown language.

## Writing Good Commit Messages

It's important to follow these guidelines when writing commit messages for this project:
1. [Semantic Commit Messages](https://seesparkbox.com/foundry/semantic_commit_messages)
2. [Commit Message Syntax](http://karma-runner.github.io/0.10/dev/git-commit-msg.html)